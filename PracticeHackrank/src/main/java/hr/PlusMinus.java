package hr;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class PlusMinus {

    // Complete the diagonalDifference function below.
    static int diagonalDifference(int[][] arr) {

        int firstDiagSum=0;
        for(int i=0;i<arr[0].length;i++){
            firstDiagSum+=arr[i][i];
        }

        int secondDiagonalSum=0;
        int j=0;

        for(int i=arr[0].length-1;i>=0;i--){
            secondDiagonalSum+=arr[j][i];
            j++;
        }
        return Math.abs(firstDiagSum-secondDiagonalSum);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {


        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < n; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }

        int result = diagonalDifference(arr);

        System.out.println(result);
        scanner.close();
    }
}
