package hr;


import java.io.*;
import java.util.*;


public class DrawingBook {

    /*
     * Complete the pageCount function below.
     */
    static int pageCount(int n, int p) {
        if(p==1 || p==n){
            return 0;
        }

        if(n%2==1){
            n--;
        }
        int numberOfMoves=n/2;

        int actualMoves=p/2;
        if(actualMoves>(numberOfMoves/2)){
            return numberOfMoves-actualMoves;
        }else {
            return actualMoves;
        }

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])*");

        int p = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])*");

        int result = pageCount(n, p);
        System.out.println(result);
        scanner.close();
    }
}
