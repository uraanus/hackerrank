package hr;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TimeConvert {

    static String timeConversion(String s) {

        int hour=Integer.parseInt(s.substring(0, 2));
        String dayOrNight=(String) s.substring(s.length()-2,s.length());
        if(dayOrNight.equals("AM")) {

            if (hour == 12) {
                return "00" + s.substring(2, s.length() - 2);
            }else {
                return s.substring(0,s.length()-2);
            }
        }else {
            if(hour == 12){
                return s.substring(0,s.length()-2);
            }else {
                hour += 12;
                String h=Integer.toString(hour);
                return h+s.substring(2, s.length() - 2);
            }
            }

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
      //  BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        String s = scan.nextLine();

        String result = timeConversion(s);
System.out.println(result);
        //bw.write(result);
        //bw.newLine();

        //bw.close();
    }
}


