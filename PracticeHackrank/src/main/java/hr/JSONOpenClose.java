package hr;

import java.io.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.net.*;
import com.google.gson.*;

public class JSONOpenClose {

    static class Page{
        String page;
        String per_page;
        String total;
        String total_pages;
        List<Stock> data;
    }

    static class Stock{
        String date;
        String open;
        String high;
        String low;
        String close;

    }
        /*
         * Complete the function below.
         */

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }



        static void openAndClosePrices(String firstDate, String lastDate, String weekDay) {


            Date fDate= null;
            Date lDate= null;
            DateFormat weekDayFormat=new SimpleDateFormat("EEEE");
            DateFormat DFormat=new SimpleDateFormat("d-MMMM-yyyy");
            int fYear=0;
            int lYear=0;
            String read=null;
            int numberOfPages=0;
            Date result=null;

            Gson gson=null;
            Page page=null;
            try {
                fDate = DFormat.parse(firstDate);
                lDate = DFormat.parse(lastDate);
                LocalDate fLocalDate = fDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate lLocalDate = lDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                 fYear=fLocalDate.getYear();
                 lYear=lLocalDate.getYear();

            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {

                for(int i = fYear;i<=lYear;i++){

                    read=readUrl("https://jsonmock.hackerrank.com/api/stocks/search?date="+i);
                    gson = new Gson();
                    page = gson.fromJson(read, Page.class);
                    numberOfPages=Integer.parseInt(page.total_pages);

                    for(int n=1;n<=numberOfPages;n++){
                        read=readUrl("https://jsonmock.hackerrank.com/api/stocks/search?date="+i+"&page="+n);
                        gson = new Gson();
                        page = gson.fromJson(read, Page.class);
                        for (Stock s: page.data){
                            result = new SimpleDateFormat("d-MMM-yyyy").parse(s.date);
                            if((result.after(fDate) || result.equals(fDate)) && (result.before(lDate) || result.equals(lDate)) )
                            {
                                if(weekDayFormat.format(result) .equals(weekDay))
                                    System.out.println(DFormat.format(result)+ " "+s.open+" "+s.close);
                            }
                        }
                    }

                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }



        public static void main(String[] args){
            Scanner in = new Scanner(System.in);
            String _firstDate;
            try {
                _firstDate = in.nextLine();
            } catch (Exception e) {
                _firstDate = null;
            }

            String _lastDate;
            try {
                _lastDate = in.nextLine();
            } catch (Exception e) {
                _lastDate = null;
            }

            String _weekDay;
            try {
                _weekDay = in.nextLine();
            } catch (Exception e) {
                _weekDay = null;
            }

            openAndClosePrices(_firstDate, _lastDate, _weekDay);

        }
    }

