package hr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Encryption {
    // Complete the encryption function below.
    static String encryption(String s) {

        String stringAfterSplit = "";
        String[] cuttered = s.split(" ");
        for (int i = 0; i < cuttered.length; i++) {
            stringAfterSplit += cuttered[i];
        }
        int lengthAfterSplit = stringAfterSplit.length();
        double sqrtOfString = Math.sqrt(lengthAfterSplit);
        int row =(int) Math.floor(sqrtOfString);
        int col =(int) Math.ceil(sqrtOfString);

        if (row * col < lengthAfterSplit) {
            row++;
        }


        ArrayList<String> afterSplittedString = new ArrayList<String>();
        for (int i = 0; i < lengthAfterSplit; i += col) {
            if (i < lengthAfterSplit && i+col < lengthAfterSplit)
                afterSplittedString.add(stringAfterSplit.substring(i, i + col));
            else
                afterSplittedString.add(stringAfterSplit.substring(i, lengthAfterSplit));
        }
        int lengthOfLastElement=afterSplittedString.get(afterSplittedString.size()-1).length();

        int spaceIndex=0;
        char result[]=new char [lengthAfterSplit+row] ;
        for (int i = 0; i < afterSplittedString.size(); i++) {
            int splittingPoint=i;
            spaceIndex=(i*col+col-1);
            result[splittingPoint]=afterSplittedString.get(i).charAt(0);
            for(int j=1;j<afterSplittedString.get(i).length();j++)
            {
                result[splittingPoint+col]=afterSplittedString.get(i).charAt(j);
                splittingPoint+=col;
            }
            result[spaceIndex]=' ';
        }

        return new String(result);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {


        String s = scanner.nextLine();

        String result = encryption(s);
        System.out.println(result);

        scanner.close();
    }
}
