package dw;

public class TransactionException extends RuntimeException {

    public static final String ERROR_CODE_USER_NOT_AUTHORIZED="USER_NOT_AUTHORIZED";
    public static final String ERROR_CODE_INSUFFICIENT_BALANCE="INSUFFICIENT_BALANCE";
    public static final String ERROR_CODE_INVALID_AMOUNT="INVALID_AMOUNT";

    private String errorCode;

    public TransactionException(String errorMessage,String errorCode){

        super(errorMessage);
        this.errorCode = errorCode;
    }

    public String getErrorCode(){
        return errorCode;
    }
}
