package dw;

public class DigitalWalletTransaction {



   public void checkAmount(DigitalWallet dw, int amount){

        if(dw.getUserAccessCode()==null){
            throw new TransactionException("User not authorized.",TransactionException.ERROR_CODE_USER_NOT_AUTHORIZED);
        }
        if(amount<=0){
            throw new TransactionException("Amount should be greater than zero.",TransactionException.ERROR_CODE_INVALID_AMOUNT);
        }

    }


    public void addMoney(DigitalWallet dw, int amount) throws TransactionException{

        checkAmount(dw, amount);
        dw.setWalletBalance(dw.getWalletBalance()+amount);

    }

    void payMoney(DigitalWallet dw,int amount) throws TransactionException{

        checkAmount(dw,amount);
        if(amount > dw.getWalletBalance()){
            throw new TransactionException("Insufficient balance.", TransactionException.ERROR_CODE_INSUFFICIENT_BALANCE);
        }

        dw.setWalletBalance(dw.getWalletBalance()-amount);

    }
}
